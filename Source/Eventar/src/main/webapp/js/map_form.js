/**
 * 
 */


google.maps.visualRefresh = true;
var map;
var geocoder;
var marker = null;
	
function initialize() {
	geocoder = new google.maps.Geocoder();
	latlng = new google.maps.LatLng(-34.397, 150.644);
	mapOptions = {
		zoom: 8,
		center: latlng
	}
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
}
	
	
google.maps.event.addDomListener(window, 'load', initialize);

function codeAddress() {
	var address = getFormattedAddress();
	geocoder.geocode( { 'address': address}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			map.setZoom(15);
		    map.setCenter(results[0].geometry.location);
		    if(marker) removeCurrentMarker();
		    addMarker(results[0].geometry.location)
		}
	});
}
	
function addMarker(location){
	marker = new google.maps.Marker({
		map: map,
        position: location
    });
}

function removeCurrentMarker(){
	marker.setMap(null);
}
	
function getFormattedAddress(){
	var addressFields = $("#address input");
	var formattedAddress = "";
		
	addressFields.each(function(index, addressField){
		var value = $(addressField).val();
		if(value){
			formattedAddress+=value;
			if(index != (addressFields.length-1)){
				formattedAddress+=", ";
			}
		}
	});
		
	return formattedAddress;
}