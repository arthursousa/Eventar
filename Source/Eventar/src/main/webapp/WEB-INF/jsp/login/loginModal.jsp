<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div id="login-modal" class="modal" style="width: 300px; height: 360px">
	
	<!-- Cabeçalho da Modal -->
 	<div class="row">
 		<div class="col s12 center orange lighten-1 center white-text" style="padding-top: 20px">
 			<img alt="Logo" src="<c:url value='/img/logo.png'/>" width="30">
 			<h4><fmt:message key="app.name"/></h4>
 		</div>
 	</div>
 
 	<!-- Conteúdo da Modal -->
 	<div class="modal-content" style="padding-top: 0px">
		
      	<form action="${linkTo[LoginController].login}" method="post">
      		<!-- Usuário -->
      		<div class="input-field col s12">
      			<i class="material-icons prefix">perm_identity</i>
      			<input type="email" name="user.username" required="true">
      			<label for="user.username"><fmt:message key="user.username"/></label>
      		</div>
      		
      		<!-- Senha -->
      		<div class="input-field col s12">
      			<i class="material-icons prefix">lock_outline</i>
      			<input type="password" name="user.password" required="true">
      			<label for="user.password"><fmt:message key="user.password"/></label>
      		</div>
      		
      		<!-- Confirmação -->
      		<button class="orange lighten-1  waves-effect waves-light btn" type="submit" style="width: 100%"><fmt:message key="app.login"/></button>
      		<!-- Link Registrar -->
      		<p class="center"><fmt:message key="default.redirectToRegisterMessage"/> <a class="orange-text lighten-1 modal-action modal-close modal-trigger" href="#form-modal"><fmt:message key="app.signup"/></a></p>
      	</form>
    </div>
</div>


