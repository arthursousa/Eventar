<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${event.title}</title>

</head>
<body>
	<c:import url="/header.jsp"></c:import>
	
	<div class="container">
		<h4 class="card-panel orange lighten-1 white-text">${event.title}</h4>
		
		<div class="row">
			<div class="col s12 m12 l6">
				<img src="/Eventar/event/image?id=${event.id}" width="100%">
			</div>
			
			<div class="col s12 m12 l6">
				<div class="row">
					<p style="margin-top:0px">${event.description}</p>
					<p style="margin-bottom: 0px"><b class="orange-text lighten-1"><fmt:message key="event.eventDate"/>: </b> <fmt:formatDate type="date" dateStyle="long" value="${event.eventDate}" /></p>
					<p style="margin-bottom: 0px; margin-top: 0px"><b class="orange-text lighten-1"><fmt:message key="event.eventLocale.label"/>: </b>${event.address.streetAddress}, ${event.address.number} - ${event.address.neighborhood}, ${event.address.city} - ${event.address.state}, ${event.address.postalCode}</p>	
					<p><fmt:message key="app.publishedBy"/> <a class="orange-text lighten-1" href="${linkTo[UserController].show}?id=${event.owner.id}">${event.owner.firstName}</p>		
				</div>

			</div>		
		</div>
	</div>
	
</body>
</html>