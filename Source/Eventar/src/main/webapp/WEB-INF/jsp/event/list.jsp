<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><fmt:message key="app.events"/></title>
</head>
<body>
	<c:import url="/header.jsp"></c:import>
	
	<c:if test="${not empty message}">
		<c:set var="message" value="${message}"/>
	</c:if>
	
	<div class="container">
		<h2 class="header orange-text lighten-1"><fmt:message key="app.events"/></h2>
		<div class="row">
			<c:import url="/cards.jsp"/>
		</div>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		Materialize.toast('${message}', 3000, 'rounded') 
	})
	</script>
</body>
</html>