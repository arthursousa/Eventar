<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  	<script type="text/javascript" src="<c:url value="/js/datepicker.js"/>"></script>  
  	<script src="http://maps.googleapis.com/maps/api/js"></script>
  	<script type="text/javascript" src="<c:url value="/js/address_autocomplete.js"/>"></script> 
  	<script type="text/javascript" src="<c:url value="/js/map_form.js"/>"></script> 
  	<link rel="stylesheet" href="<c:url value="/css/materialized.css"/>">
	<title><fmt:message key="event.register.edit"></fmt:message></title>
</head>
<body>
	<!-- Cabeçalho -->
	<c:import url="/header.jsp"/>
	
	<div class="container">
		<form action="save" method="post" method="post" enctype="multipart/form-data">
			<!-- Detalhes do Evento -->
			<div id="eventInfo" class="row">
				<div class="col s12 m12 l12">
					<h5 class="card-panel orange lighten-1 white-text"><fmt:message key="event.eventDetail.label"/></h5>
					<div class="card-panel">

						<div class="row">
						
							<input name="event.id" value="${event.id}" type="hidden">
							<!-- Título -->
							<div class="input-field col s12 m12 l8">
								<input type="text" name="event.title" value ="${event.title}" required="true"> <label
									for="event.title"><fmt:message key="event.title"/></label>
							</div>

							<!-- Dia do Evento -->
							<div class="input-field col s12 m12 l4">
								<label for="event.enventDate" class="active"><fmt:message key="event.eventDate"/></label> 
								<input name="event.eventDate" value='<fmt:formatDate pattern="dd/MM/yyyy" value="${event.eventDate}"/>'  type="date" class="datepicker" required="true">
							</div>
						</div>

						<!-- Descrição -->
						<div class="row">
							<div class="input-field col s12 m12">
								<textarea name="event.description" type="text" required="true" class="materialize-textarea">${event.description}</textarea>
								<label for="event.description"><fmt:message key="event.description"/></label>
							</div>
						</div>

						<!-- Imagem -->
						<div class="row">
							<div class="file-field input-field col s12 m12">
								<div class="btn orange lighten-1 white-text">
									<span><fmt:message key="event.image.upload.label"/></span> <input name="image" type="file" accept="image/*">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text" placeholder="<fmt:message key="event.image.placeholder.label"/>" disabled>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			
			<!-- Local do Evento -->
			<div id="address" class="row"> 
				<div class="col s12 m12 l12">
					<h5 class="card-panel orange lighten-1 white-text"><fmt:message key="event.eventLocale.label"/></h5>
					<div class="card">
						<!-- Mapa -->
						<div class="card-image waves-effect waves-block waves-light">
							<div id="map-canvas"></div>
                		</div>
                		<!-- Endereço -->
                		<div class="card-content">
							<div class="row">
								<!-- CEP -->
								<div class="input-field col s12 m12 l10">
									<input type="text" id="postalCode" name="event.address.postalCode" value="${event.address.postalCode}" type="text" required="true"> 
									<label for="event.address.postalCode"><fmt:message key="address.postalCode"/></label>
								</div>
								
								<!-- Número -->
								<div class="input-field col s12 m12 l2">
									<input id="number" name="event.address.number" value="${event.address.number}" type="number" required="true" onblur="codeAddress()">
									<label for="event.address.number"><fmt:message key="address.number"/></label>							
								</div>
							</div>
							
							<!-- Rua -->
							<div class="row">
								<div class="input-field col s12 m12 l12">
									<input type="text" id="streetAddress" name="event.address.streetAddress"  value="${event.address.streetAddress}" required="true" onblur="codeAddress()">
									<label for="event.address.postalCode"><fmt:message key="address.streetAddress"/></label>
								</div>
							</div>
							
							<!-- Bairro -->
							<div class="row">
								<div class="input-field col s12 m12 l12">
									<input type="text" id="neighborhood" name="event.address.neighborhood" value = "${event.address.neighborhood}" required="true" onblur="codeAddress()">
									<label for="event.address.neighborhood"><fmt:message key="address.neighborhood"/></label>
								</div>
							</div>
							
							<div class="row">
								<!-- Cidade -->
								<div class="input-field col s12 m12 l10">
									<input type="text" id="city" name="event.address.city" value="${event.address.city}" required="true" onblur="codeAddress()">
									<label for="event.address.city"><fmt:message key="address.city"/></label>
								</div>
								
								<!-- UF -->
								<div class="input-field col s12 m12 l2">
									<input type="text" id="state" name="event.address.state" value="${event.address.state}" required="true" onblur="codeAddress()">
									<label for="event.address.state"><fmt:message key="address.state"/></label>							
								</div>
							</div>
							
						</div>                		
                	</div>

				</div>
			</div>
			
			
			<!-- Confirmação do Formulário -->
			<div class="row">
				<div class="col s12 m12 l12">					     
					<button class="orange lighten-1 white-text waves-effect waves-light btn right" type="submit">
						<fmt:message key="default.submit"/>
					</button>
				</div>	
			</div>
		</form>
	</div>
	

</body>
</html>