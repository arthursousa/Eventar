<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title><fmt:message key="app.userManagement"/></title>
</head>
<body>
	<c:import url="/header.jsp"></c:import>
	
	<c:if test="${not empty message}">
		<c:set var="message" value="${message}"/>
	</c:if>
	
	<div class="container">
		<table class="bordered centered striped" style="margin-bottom:10px">
	        <thead>
	          <tr>
	              <th data-field="id"><fmt:message key="user.id"/></th>
	              <th data-field="username"><fmt:message key="user.username"/></th>
	              <th data-field="name"><fmt:message key="user.name"/></th>
	              <th data-field="lastName"><fmt:message key="user.lastName"/></th>
	              <th data-field="password"><fmt:message key="user.password"/></th>
	          </tr>
	        </thead>
	
	        <tbody>
	         <c:forEach items="${userList}" var="user">
	          <tr>
	            <td>${user.id}</td>
	            <td>${user.username}</td>
	            <td>${user.firstName}</td>
	            <td>${user.lastName}</td>
	            <td>${user.password.substring(0,20)}...</td>
	            <td>
	              	<a class="btn-floating btn-medium orange lighten-1 modal-trigger" href="${linkTo[UserController].show}?id=${user.id}">
	            		<i class="medium material-icons">info</i>
	    			</a>
	            	<a class="btn-floating btn-medium orange lighten-1" href="${linkTo[UserController].delete}?id=${user.id}">
	            		<i class="medium material-icons">delete</i>
	    			</a>
	    		</td>
	          </tr>
	         </c:forEach>
	        </tbody>
	      </table>
	      
	      <ul class="pagination right">
	  	  		<li class="waves-effect"><i class="material-icons">chevron_left</i></li>
			    <li class="active orange lighten-1"><a href="${linkTo[UserController].list()}?page=1">1</a></li>
			    <li class="active orange lighten-1"><a href="${linkTo[UserController].list()}?page=2">2</a></li>
			    <li class="active orange lighten-1"><a href="${linkTo[UserController].list()}?page=3">3</a></li>
			    <li class="active orange lighten-1"><a href="${linkTo[UserController].list()}?page=4">4</a></li>
			  	<li class="waves-effect"><i class="material-icons">chevron_right</i></li>
 		  </ul>
	      
	      <a class="btn-floating btn-medium orange lighten-1 modal-trigger" href="#form-modal">
	    	 <i class="small material-icons">add</i>
	  	  </a>
	</div>
	
	<script type="text/javascript">
	$(document).ready(function(){
		Materialize.toast('${message}', 3000, 'rounded') 
	})
	</script>
	
<c:import url="/WEB-INF/jsp/user/formModal.jsp"></c:import>
</body>
</html>