<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<div id="form-modal" class="modal" style="width: 450px; height: 420px">
 	<!-- Cabeçalho da Modal -->
 	<div class="row">
 		<div class="col s12 center orange lighten-1 center white-text" style="padding-top:20px">
 			<img alt="Logo" src="<c:url value='/img/logo.png'/>" width="30">
 			<h4><fmt:message key="app.name"/></h4>
 		</div>
 	</div>
 	
 	<!-- Conteúdo da Modal -->
	<div class="modal-content" style="padding-top: 0px">
		<form  action="${linkTo[UserController].save}"  method="post"  class="col s12 m6">
	    	<div class="row" style="height: 10px">
	    		<!-- Nome -->
	        	<div class="input-field col s6">
	            	<input name="user.firstName" id="first_name" type="text" class="validate" required="true">
	                <label for="first_name"><fmt:message key="user.name"/></label>
	            </div>
	        	<!-- Sobrenome -->
	        	<div class="input-field col s6">
	                <input name="user.lastName" id="last_name" type="text" class="validate" required="true">
	                <label for="last_name"><fmt:message key="user.lastName"/></label>
	            </div>
	        </div>
	        
	        <!-- Senha -->
	        <div class="row" style="height: 10px">
	        	<div class="input-field col s12">
	            	<input name="user.password" id="password" type="password" class="validate" required="true">
	                <label for="password"><fmt:message key="user.password"/></label>
	            </div>
	            
	        </div>
	        
	        <!-- Email -->
	        <div class="row" style="height: 10px">
	        	<div class="input-field col s12">
	            	<input name="user.username" id="email" type="email" class="validate" required="true">
	                <label for="email"><fmt:message key="user.username"/></label>
	            </div>
	        </div>
	        <!-- Submit -->
	        <button class="orange lighten-1  waves-effect waves-light btn" type="submit" style="width: 100%"><fmt:message key="default.submit"/></button>
	        </form>
	</div>	
</div>