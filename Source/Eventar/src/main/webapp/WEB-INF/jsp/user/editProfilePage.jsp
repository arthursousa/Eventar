<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title><fmt:message key="app.editUserProfile"/></title>
	<link rel="stylesheet" href="<c:url value="/css/materialize_admin_template.css"/>">
</head>
<body>
	<c:import url="/header.jsp"></c:import>
	
	<div class="container" style="padding-left:100px">
		<form action="${linkTo[UserController].update}"  method="post">
			<div id="userProfile" class="row">
				<div class="col s10 m10 l10">
					<h5 class="card-panel center orange lighten-1 white-text"><fmt:message key="app.editUserProfile"/></h5>
					<div class="card-panel">

						<div class="row">
				        	<div class="col s8">
				        		<div class="row">
				        			<input name="user.id" value=${user.id} type="hidden" />
				        			<!-- Name -->
				        			<div class="input-field col s6">
				            			<input name="user.firstName" id="first_name" type="text" class="validate" required="true" value =${user.firstName}>
				                		<label for="first_name"><fmt:message key="user.name"/></label>
				                	</div>
				        			<!-- LastName -->
				        			<div class="input-field col s6">
				                		<input name="user.lastName" id="last_name" type="text" class="validate" required="true" value=${user.lastName}>
				                		<label for="last_name"><fmt:message key="user.lastName"/></label>
				                	</div>
				                </div>
				                
				                <div class="row">
					                <!-- Email -->
					                <div class="input-field col s6">
					                	<input name="user.username" id="email" type="email" class="validate" required="true" value=${user.username}>
					                	<label for="email"><fmt:message key="user.username"/></label>
					            	</div>
					            	
					                <!-- Password -->
				                	<div class="input-field col s6">
				            			<input name="user.password" id="password" type="password" class="validate" required="true">
				                		<label for="password"><fmt:message key="user.password"/></label>
				               		</div>
					            </div>
					            
					            <!-- <div class="row">
									<div class="file-field input-field col s12 m12">
										<div class="btn orange lighten-1 white-text">
											<span><fmt:message key="app.upload"/></span> <input name="image" type="file" accept="image/*" width="300" height="200">
										</div>
										<div class="file-path-wrapper">
											<input class="file-path validate" type="text" value="${event.image}" placeholder="<fmt:message key="app.insertProfilePicture"/>" disabled>
										</div>
									</div>
								</div> -->
				            </div>

				            <!-- User Picture -->
				           	<div class="col s2">
			            		<img src="https://upload.wikimedia.org/wikipedia/commons/4/41/AWS_Simple_Icons_Non-Service_Specific_User.svg" style="width:250px; height:250px">
			        		</div>
						</div>

						<!-- <div class="row">
							<div class="input-field col s12 m12">
								<textarea name="user.additionalInfo" type="text" class="materialize-textarea">${event.description}</textarea>
								<label for="event.description"><fmt:message key="user.additionalInfo"/></label>
							</div>
						</div> -->
						
						<!-- Confirmação do Formulário-->				     
 						<button class="orange lighten-1 waves-effect waves-light btn" type="submit" style="width:100%"><fmt:message key="app.done"/></button>
					</div>
				</div>
			</div>
		</form>
	</div>
	
</body>
</html>