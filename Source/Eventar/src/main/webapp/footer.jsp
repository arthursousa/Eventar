<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body>
<footer class="page-footer orange lighten-1" style="height:610px">
      <div class="container">
        <div class="row">
          <div class="col l6 s16">
            <h5 class="white-text"><fmt:message key="app.getInTouch"/></h5>
            <div class="row">
              <form class="col 16 s16">
                  <div class="row">
	                  <div class="input-field col 6 s6">
	                    <i class="material-icons prefix white-text ">account_circle</i>
	                    <input id="name" type="text" class="white-text">
	                    <label for="name" class="white-text"><fmt:message key="user.name"/></label>
	                  </div>
	                  <div class="input-field col 6 s6">
	                    <i class="material-icons prefix white-text">email</i>
	                    <input id="mail" type="email" class="white-text">
	                    <label for="mail" class="white-text"><fmt:message key="user.username"/></label>
	                  </div>
	              </div>
	                  <div class="input-field col 12 s12">
	                  	<div id="contact" class="section scrollspy">
	                    	<i class="material-icons prefix white-text">message</i>
	                    	<textarea id="message" type="text" class="materialize-textarea white-text"></textarea>
	                    	<label for="message" class="white-text"><fmt:message key="app.message"/></label>
	                  	</div>
					  </div>
	                  <button class="btn waves-effect waves-light white orange-text text-lighten-1 right" type="submit">
	                    <fmt:message key="app.send"/>
						<i class="mdi-content-send right text-ligthen-1"></i>
					  </button>
              </form>
            </div>
          </div>
          <div class="col l6 s16" style="text-align: right">
             <h5 class="white-text" style="margin-bottom: 0px">Arthur Borges</h5>
              <ul style="margin-top: 0px">
                <li>
                  <a href="arthurbosousa@gmail.com" target="_blank"><i class="fa fa-envelope fa-2x right" style="color:white;margin-top:10px;"></i></a>
                  <a href="https://br.linkedin.com/in/sousarthur/pt" target="_blank"><i class="fa fa-linkedin-square fa-2x right" style="color:white; margin-top:10px"></i></a>
                  <a href="https://www.facebook.com/arthur.borgesos" target="_blank"><i class="fa fa-facebook-official fa-2x right" style="color:white;margin-top:10px;"></i></a>
                </li>
              </ul>
              <br/>
              <h5 class="white-text" style="margin-bottom: 0px">Lucas Weyne</h5>
              <ul style="margin-top: 0px">
                <li>
                  <a href="weynelucas@gmail.com" target="_blank"><i class="fa fa-envelope fa-2x right" style="color:white;margin-top:10px;"></i></a>
                  <a href="https://br.linkedin.com/in/lucas-weyne-barros-ferreira-409709ba" target="_blank"><i class="fa fa-linkedin-square fa-2x right" style="color:white; margin-top:10px"></i></a>
                  <a href="https://www.facebook.com/weynelucas" target="_blank"><i class="fa fa-facebook-official fa-2x right" style="color:white;margin-top:10px;"></i></a>
                </li>
              </ul>
          </div>
        </div>
      </div>
      
      
      <div class="footer-copyright" style="height:225px" >
        <div class="container">
        	<fmt:message key="app.copyright"/>
        </div>
      </div>
    </footer>
</body>
</html>