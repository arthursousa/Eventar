package br.com.web2015.eventar.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.web2015.eventar.model.dao.EventDao;

@Controller
public class IndexController {
	private static final int MAX_RESULTS = 3;
	
	@Inject
	private EventDao dao;
	
	@Inject
	private Result result;
	
	@Path("/")
	public void index(){
		result.include("eventList", dao.list(MAX_RESULTS));
	}

}
