package br.com.web2015.eventar.model.entity;


import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;



@Entity
@Table(name="eventar_event")
public class Event {
	
	@Id @GeneratedValue
	private Long id;
	
	private String title;
	
	@Column(length = 1000)
	private String description;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="publication_date")
	private Date publicationDate;
	
	@Column(name="event_Date")
	private Date eventDate;
	
	private byte[] image;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="address_id")
	private Address address;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="owner_id")
	private User owner;
	
	public Long getId() {
		return id;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public byte[] getImage() {
		return image;
	}
	
	public Date getPublicationDate() {
		return publicationDate;
	}
	
	public Date getEventDate() {
		return eventDate;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public User getOwner() {
		return owner;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setImage(byte[] image) {
		this.image = image;
	}
	
	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public void setOwner(User owner) {
		this.owner = owner;
	}
	
}
