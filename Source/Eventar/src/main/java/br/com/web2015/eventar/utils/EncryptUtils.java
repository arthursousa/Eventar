package br.com.web2015.eventar.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptUtils {
	private static final String ENCRYPT_ALGORITH = "SHA-256";
	private static final String ENCODING_TYPE = "UTF-8";
	
	public static String encryt(String original){
		try {
			MessageDigest algorithm = MessageDigest.getInstance(ENCRYPT_ALGORITH);
			byte messageDigest[];
			messageDigest = algorithm.digest(original.getBytes(ENCODING_TYPE));
			
			StringBuilder hexString = new StringBuilder();
			for (byte b : messageDigest) {
			  hexString.append(String.format("%02X", 0xFF & b));
			}
			return hexString.toString();
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Characters codification type not allowed", e);
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException("Encryption algorithm not found", e);
		}
	}
}
