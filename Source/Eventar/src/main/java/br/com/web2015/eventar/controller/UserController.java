package br.com.web2015.eventar.controller;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.web2015.eventar.annotation.Authenticate;
import br.com.web2015.eventar.annotation.EncryptPassword;
import br.com.web2015.eventar.component.UserSession;
import br.com.web2015.eventar.model.dao.UserDao;
import br.com.web2015.eventar.model.entity.User;

@Controller
public class UserController {
	
	public static final int INITIAL_PAGE = 1;
	
	@Inject
	private UserDao dao;
	
	@Inject
	private UserSession userSession;
		
	@Inject 
	private Result result;
	
	public void profilePage(){}
	
	public void editProfilePage(){}
	
	@Post
	@EncryptPassword
	public void save(User user){
		if(dao.find(user) == null){
			
			dao.save(user);
			
			if(userSession.isLoggedIn()){
				result.include("message", "User successfully registered");
				result.redirectTo(this).list(INITIAL_PAGE);
			} else {
				userSession.login(user);
				result.redirectTo(EventController.class).list();	
			}
		} else {
			if(userSession.isLoggedIn()){
				result.include("message", "User already registered");
				result.redirectTo(this).list(INITIAL_PAGE);
			} else {
				result.forwardTo(IndexController.class).index();
			}
		}
	}
	
	@Authenticate
	public void show(Long id){
		if( id == 0) { // Mostra o perfil do próprio usuário
			result.include("user", userSession.getCurrentUser());
			result.forwardTo(this).profilePage();
		} else {
			result.include("user", dao.retrieve(id));
			result.forwardTo(this).profilePage();
		}
	}

	@Authenticate
	public void edit(Long id){
		result.include("user", dao.retrieve(id));
		result.forwardTo(this).editProfilePage();
	}

	@Post
	@EncryptPassword
	@Authenticate
	public void update(User user){
		dao.update(user);
		if(userSession.getCurrentUser().getUsername().equals("admin@mail.com")){
			result.include("message", "User successfully edited");
			result.redirectTo(this).list(INITIAL_PAGE);
		} else {
			result.include("user", dao.find(user));
			result.redirectTo(this).profilePage();
		}
	}
	
	@Authenticate
	public void delete(Long id){
		dao.delete(dao.retrieve(id));
		result.include("message", "User successfully deleted");
		result.forwardTo(this).list(INITIAL_PAGE);
	}
	
	@Authenticate
	public List<User> list(int page){
		if(userSession.getCurrentUser().getUsername().equals("admin@mail.com")){
			return dao.list(page);
		} else {
			result.redirectTo(EventController.class).list();
			return null;
		}
	}
}
