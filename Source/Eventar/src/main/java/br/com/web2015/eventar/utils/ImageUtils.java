package br.com.web2015.eventar.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.apache.commons.fileupload.util.Streams;

import br.com.caelum.vraptor.observer.upload.UploadedFile;
import br.com.web2015.eventar.model.entity.Event;

public class ImageUtils {
	
	
	public static void uploadEventImage(UploadedFile uploadedFile, Event event){
		
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			Streams.copy(uploadedFile.getFile(), out, false);
			
			event.setImage(out.toByteArray());
		} catch (IOException e) {
			throw new RuntimeException("Error when copying file", e);
		}
	}

}
