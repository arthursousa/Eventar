package br.com.web2015.eventar.component;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.com.web2015.eventar.model.entity.User;

@SessionScoped
@Named("userSession")
public class UserSession implements Serializable{

	private static final long serialVersionUID = -4795427877228508131L;
	private User currentUser;
	
	public void login(User user){
		this.currentUser = user;
	}
	
	public void logout(){
		this.currentUser = null;
	}
	
	public User getCurrentUser(){
		return this.currentUser;
	}
	
	public boolean isLoggedIn(){
		return this.currentUser != null;
	}
}
