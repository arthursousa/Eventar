package br.com.web2015.eventar.interceptor;


import javax.inject.Inject;

import br.com.caelum.vraptor.Accepts;
import br.com.caelum.vraptor.AroundCall;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.interceptor.SimpleInterceptorStack;
import br.com.web2015.eventar.annotation.Authenticate;
import br.com.web2015.eventar.component.UserSession;
import br.com.web2015.eventar.controller.IndexController;
import br.com.web2015.eventar.controller.LoginController;
import br.com.web2015.eventar.controller.UserController;

@Intercepts
public class AuthenticateInterceptor {
	
	@Inject UserSession userSession;
	
	@Inject Result result;
	
	@Accepts
	public boolean accepts(ControllerMethod method){
		return method.containsAnnotation(Authenticate.class);
	}
	
	@AroundCall
	public void intercept(SimpleInterceptorStack stack){
		if(userSession.isLoggedIn()){
			stack.next();
		} else{
			result.redirectTo(IndexController.class).index();
		}
	}
	
}
