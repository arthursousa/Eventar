package br.com.web2015.eventar.controller;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.web2015.eventar.annotation.Authenticate;
import br.com.web2015.eventar.annotation.EncryptPassword;
import br.com.web2015.eventar.component.UserSession;
import br.com.web2015.eventar.model.dao.UserDao;
import br.com.web2015.eventar.model.entity.User;

@Controller
public class LoginController {
	
	public static final int INITIAL_PAGE = 1;
	
	@Inject
	private UserDao dao;
	
	@Inject
	private UserSession userSession;
	
	
	@Inject 
	private Result result;
	
	@Post("/login")
	@EncryptPassword
	public void login(User user){
		User loadedUser = dao.load(user);
		
		if(loadedUser == null){
			result.include("message", "User and/or password invalid");
			result.redirectTo(IndexController.class).index();
		} else if(loadedUser.getUsername().equals("admin@mail.com")){
			userSession.login(loadedUser);
			result.include("message", "User successfully logged in");
			result.redirectTo(UserController.class).list(INITIAL_PAGE);
		} else {
			userSession.login(loadedUser);
			result.include("message", "User successfully logged in");
			result.redirectTo(EventController.class).list();	
		}
	}
	
	@Authenticate
	public void logout(){
		userSession.logout();
		result.redirectTo(IndexController.class).index();
	}
}
