package br.com.web2015.eventar.model.dao;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.com.web2015.eventar.model.entity.User;
@RequestScoped
public class UserDao {
	
	public static final int USERS_SHOWN = 10;
	
	@Inject
	private Session session;
	
	private Criteria createCriteria(){
		return session.createCriteria(User.class);
	}
	
	public void saveOrUpdate(User user){
		session.saveOrUpdate(user);
	}
	
	public void save(User user){
		session.save(user);
	}
	
	public void delete(User user){
		session.delete(user);
	}
	
	public void update(User user){
		session.update(user);
	}
	
	public User find(User user) {
		return (User) createCriteria()
				.add(Restrictions.eq("username", user.getUsername()))
				.uniqueResult();
	}

	public User retrieve(Long id) {
		return (User) session.load(User.class, id);
	}

	public User load(User user) {
		return (User) createCriteria()
				.add(Restrictions.eq("username", user.getUsername()))
				.add(Restrictions.eq("password", user.getPassword()))
				.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	public List<User> list(int page){
		return createCriteria().setMaxResults(USERS_SHOWN).setFirstResult(((page-1)*USERS_SHOWN)).addOrder(Order.asc("id")).list();
	}
}
