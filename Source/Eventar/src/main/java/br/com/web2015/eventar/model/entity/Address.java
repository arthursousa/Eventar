package br.com.web2015.eventar.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="eventar_address")
public class Address {
	
	@Id @GeneratedValue
	private Long id;
	
	@Column(name="postal_code")
	private String postalCode;
	
	private String neighborhood;
	
	private String city;
	
	private String state;
	
	@Column(name="street_address")
	private String streetAddress;
	
	private String country;
	
	private int number;
	
	public Long getId() {
		return id;
	}
	
	public String getPostalCode() {
		return postalCode;
	}
	
	public String getCity() {
		return city;
	}
	
	public String getNeighborhood() {
		return neighborhood;
	}
	
	public String getCountry() {
		return country;
	}
	
	public String getState() {
		return state;
	}
	
	public String getStreetAddress() {
		return streetAddress;
	}
	
	public int getNumber() {
		return number;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setNeighborhood(String neighborhood) {
		this.neighborhood = neighborhood;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	
	public void setNumber(int number) {
		this.number = number;
	}
	
}
